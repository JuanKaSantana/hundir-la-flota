export const getDefaultGame = (idPlayer, displayName) => {
    const players = {}
    players[idPlayer] = getDefaultPlayer(displayName)

    return {
        players,
        chat: [{ playerId: 'System', message: 'Bienvenidos al chat' }]
    }
}

export const getDefaultBoard = () => {
    return [
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ]
}

export const getDefaultPlayer = (displayName) => {
    return {
        ready: false,
        displayName
    }
}