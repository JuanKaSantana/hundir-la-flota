import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'

const config = {
  apiKey: "AIzaSyAXEixnO2mD69rq6iF9HuuqpeJCb8KgMQo",
  authDomain: "hundir-los-barcos.firebaseapp.com",
  projectId: "hundir-los-barcos",
  storageBucket: "hundir-los-barcos.appspot.com",
  messagingSenderId: "132982029881",
  appId: "1:132982029881:web:b77ef03a73d07d7c9297e8",
  measurementId: "G-Y9M06HW9DB"
}

if (!firebase.apps.length) {
  firebase.initializeApp(config)
}

const auth = firebase.auth();
const database = firebase.database();

export {
  auth, database, firebase
}
