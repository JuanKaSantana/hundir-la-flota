function Button({ title, onClick, extraClass = '' }) {
    return (
        <div className={`button ${extraClass}`} onClick={onClick}>
            {title}
        </div>
    )
}

export default Button