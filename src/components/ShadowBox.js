function ShadowBox({ children, extraClass = '' }) {
    return (
        <div className={`shadowbox ${extraClass}`}>
            {children}
        </div>
    )
}

export default ShadowBox;
