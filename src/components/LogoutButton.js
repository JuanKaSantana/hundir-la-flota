import { useState } from 'react';
import { Loading, Button } from './';
import { auth } from '../firebaseConfig';

function LogoutButton() {
    const [loading, setLoading] = useState(false)
    const logout = () => {
        setLoading(true)
        auth.signOut()
            .then(() => {
                setLoading(false)
                window.location = '/login'
            })
    }

    if (loading) {
        return <Loading />
    }

    return (
        <Button title="Cerrar Sesión" extraClass="logout-button no-border" onClick={logout} />
    )
}

export default LogoutButton;
