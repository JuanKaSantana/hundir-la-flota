import { Link } from 'react-router-dom';
import { LogoutButton } from './';

function Menu({ }) {
    return (
        <div className="menu-container">
            <div className="menu-content">
                <Link to="/home">Jugar</Link>
                <Link to="/profile">Perfil</Link>
                <LogoutButton />
            </div>
        </div>
    )
}

export default Menu;
