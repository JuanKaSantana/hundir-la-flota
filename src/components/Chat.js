import { useState, useEffect } from 'react'
import { database } from '../firebaseConfig';
import { Button, ShadowBox } from './';

function Chat({ playerId, gameId }) {
    const [chat, setChat] = useState([])
    const [message, setMessage] = useState('')
    const audio = new Audio('../assets/message.mp3');

    useEffect(() => {
        database.ref(`games/${gameId}`).child('chat')
            .on('value', (snapshot) => {
                const chatArray = snapshot.val()
                setChat(chatArray)
            })
    }, [database])

    useEffect(() => {
        const lastMessage = chat[chat.length - 1]
        if (lastMessage && lastMessage.playerId && lastMessage.playerId !== playerId) {
            audio.play()
        }
        const objDiv = document.getElementById('chat-content');
        objDiv.scrollTop = objDiv.scrollHeight;
    }, [chat])

    const handleSend = () => {
        chat.push({
            playerId,
            message
        })
        database.ref(`games/${gameId}`).child('chat')
            .set(chat)
            .then(() => {
                setMessage('')
            })
    }

    const handleMessageChange = (evt) => {
        const message = evt.target.value;
        console.log({evt})
        if (message && message.length > 0) {
            setMessage(message)
        }
    }

    if (!chat) {
        return <div />
    }
    return (
        <ShadowBox extraClass="chat-container w-250">
            <div className="chat-content" id="chat-content">
                {
                    chat && chat.length > 0 && chat.map((chatObj, index) => (
                        <div key={`message-${playerId}-${index}`} className={`chat-message ${playerId === chatObj.playerId && 'own-message'}`}>
                            <span>{chatObj.message}</span>
                        </div>
                    ))
                }
            </div>
            <div className="chat-input-container">
                <input type="text" onChange={(evt) => handleMessageChange(evt)} value={message} />
                <Button title="Enviar" onClick={handleSend} />
            </div>
        </ShadowBox>
    )
}

export default Chat
