import { useState, useEffect } from 'react';
import { database } from '../firebaseConfig';
import { getDefaultBoard } from '../helpers/defaultGame';

function Board({
    idGame,
    idPlayer,
    selectedMarker,
    selectedPiece,
    idBoard
}) {
    const [game, setGame] = useState(null)
    const [board, setBoard] = useState(getDefaultBoard())
    window.oncontextmenu = function () {
        return false;
    }
    useEffect(() => {
        database.ref(`games/${idGame}`)
            .on('value', (snapshot) => {
                setGame(snapshot.val())
            })
    }, [database])

    const handleBoardClick = (id, e) => {
        if (selectedPiece) {
            document.getElementById(id).style.backgroundColor = selectedPiece.color
        } else if (selectedMarker) {
            document.getElementById(id).style.backgroundColor = selectedMarker.color
        }
    }
    const letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']

    return (
        <div className="board-container">
            <div className="board-row header-row">
                <div className="board-col header-col"></div>
                <div className="board-col header-col">1</div>
                <div className="board-col header-col">2</div>
                <div className="board-col header-col">3</div>
                <div className="board-col header-col">4</div>
                <div className="board-col header-col">5</div>
                <div className="board-col header-col">6</div>
                <div className="board-col header-col">7</div>
                <div className="board-col header-col">8</div>
                <div className="board-col header-col">9</div>
                <div className="board-col header-col">10</div>
            </div>
            {
                board && board.length > 0 && board.map((boardRow, indexRow) => (
                    <div className="board-row" id={`${idBoard}-row-${indexRow}`} key={`${idBoard}-row-${indexRow}`}>
                        <div className={`board-col letter-col `}>{letters[indexRow]}</div>
                        {
                            boardRow && boardRow.length > 0 && boardRow.map((boardCol, indexCol) => (
                                <div onClick={(evt) => handleBoardClick(`${idBoard}-row-${indexRow}-col-${indexCol}`, evt)} className={`board-col ${board.length - 1 === indexRow && 'border-bottom'}`} id={`${idBoard}-row-${indexRow}-col-${indexCol}`} key={`${idBoard}-row-${indexRow}-col-${indexCol}`}></div>
                            ))
                        }
                    </div>
                ))
            }
        </div>
    )
}

export default Board;
