function Loading() {
    return (
        <div className="loading">
            <span className="loading-inner-1"></span>
            <span className="loading-inner-2"></span>
            <span className="loading-inner-3"></span>
        </div>
    )
}

export default Loading;
