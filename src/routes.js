import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import App from './App';
import {
    Login,
    Home,
    Profile,
    Game
} from './screens';

function AppRoutes() {
    return (
        <App>
            <Switch>
                <Route
                    exact
                    path="/login"
                    component={Login}
                />
                <Route
                    exact
                    path="/home"
                    component={Home}
                />
                <Route
                    exact
                    path="/profile"
                    component={Profile}
                />
                <Route
                    exact
                    path="/game/:id"
                    component={Game}
                />

                <Redirect exact from="/" to="/login" />
                <Redirect from="*" to="/login" />
            </Switch>
        </App>
    )
}

export default AppRoutes;
