import Login from './Login';
import Home from './Home';
import Profile from './Profile';
import Game from './Game';

export {
    Login,
    Home,
    Profile,
    Game
}