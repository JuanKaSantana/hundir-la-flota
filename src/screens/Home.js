import { useState } from 'react';
import { auth, database } from '../firebaseConfig';
import { getDefaultGame, getDefaultBoard, getDefaultPlayer } from '../helpers/defaultGame';
import { Button, Input, Loading, Menu } from '../components';

function Home() {
    const [loading, setLoading] = useState(true)
    const [idPlayer, setIdPlayer] = useState(null)
    const [namePlayer, setNamePlayer] = useState('')
    const [idGame, setIdGame] = useState('')
    auth.onAuthStateChanged(function (user) {
        if (!user) {
            window.location = "/login"
            return;
        } else {
            setIdPlayer(user.uid)
            setNamePlayer(user.displayName)
        }
        setLoading(false)
    });
    const createGame = () => {
        const gameId = `${parseInt(Math.random() * 1000000000000000000000, 10)}`
        const game = getDefaultGame(idPlayer, namePlayer)
        database.ref(`games/${gameId}`).set(game)
            .then(() => {
                window.location = `game/${gameId}`
            })
    }
    const joinGame = () => {
        database.ref(`games/${idGame}`).on('value', (snapshot) => {
            if (!snapshot.val()) {
                alert('Partida no encontrada')
            } else {
                const game = snapshot.val()
                if (Object.keys(game.players).length === 2 && !Object.keys(game.players).includes(idPlayer)) {
                    alert('Partida llena')
                    return
                }
                if (!Object.keys(game.players).includes(idPlayer)) {
                    game.players[idPlayer] = getDefaultPlayer(namePlayer)
                    database.ref(`games/${idGame}`).set(game)
                        .then(() => {
                            window.location = `game/${idGame}`
                        })
                }
            }
        })
    }
    if (loading) {
        return <Loading />
    }
    return (
        <div className="home">
            <Menu />
            <div className="home-content">
                <Button title="Crear sala" onClick={createGame} />
                <div className="game-searcher-container">
                    <label>ID de sala:</label>
                    <Input onChange={setIdGame} value={idGame} maxLength={50} extraClass="w-300" />
                </div>
                <Button title="Unirse a una sala" onClick={joinGame} extraClass="mt-20" />
            </div>
        </div>
    )
}

export default Home;
