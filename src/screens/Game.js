import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom'
import { Board, Button, Chat, ShadowBox } from '../components';
import { database, auth } from '../firebaseConfig';

const copy = (str) => {
    const el = document.createElement('textarea');
    el.value = str;
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
}

const removeGame = (gameId) => {
    database.ref(`games/${gameId}`).remove()
}

export default function Game() {
    const [showBoard, setShowBoard] = useState(true)
    const [idPlayer, setIdPlayer] = useState(null);
    const [players, setPlayers] = useState({})
    const [pieces, setPieces] = useState({})
    const [markers, setMarkers] = useState({})
    const [selectedMarker, setSelectedMarker] = useState(null)
    const [selectedPiece, setSelectedPiece] = useState(null)
    const [playing, setPlaying] = useState(false)
    const { id } = useParams();

    auth.onAuthStateChanged(async (user) => {
        if (!user) {
            window.location = "/login"
            return;
        } else {
            setIdPlayer(user.uid)
        }
    });
    database.ref(`games/${id}`).on('value', (snapshot) => {
        if (!snapshot.val()) {
            window.location = "/home"
            return;
        }
    })
    useEffect(() => {
        if (Object.keys(players).length < 2) {
            database.ref(`games/${id}`).child('players')
                .on('value', (snapshot) => {
                    setPlayers(snapshot.val())
                })
        }
        database.ref(`pieces`)
            .on('value', (snapshot) => {
                setPieces(snapshot.val())
            })
        database.ref(`markers`)
            .on('value', (snapshot) => {
                setMarkers(snapshot.val())
            })
    }, [database])

    const handleReset = () => {
        setSelectedMarker(null)
        setSelectedPiece(null)
        setShowBoard(false);
        setPlaying(false)
        setShowBoard(true)
    }
    const handlePlay = () => {
        setSelectedMarker(null)
        setSelectedPiece(null)
        setPlaying(true)
    }

    return (
        <div className="game">
            <div className="game-content">
                <div className="game-id-container">
                    <ShadowBox extraClass="w-200">
                        El ID de la sesión es: {id} <Button title="Copiar" onClick={() => copy(id)} />
                    </ShadowBox>
                </div>
                {
                    showBoard && (
                        <Board
                            idBoard="ally"
                            idGame={id}
                            idPlayer={idPlayer}
                            selectedPiece={selectedPiece}
                            selectedMarker={selectedMarker}
                        />
                    )
                }
                {
                    showBoard && (
                        <Board
                            idBoard="enemy"
                            idGame={id}
                            idPlayer={idPlayer}
                            selectedPiece={selectedPiece}
                            selectedMarker={selectedMarker}
                        />
                    )
                }
                {
                    Object.keys(players).length > 0 && (
                        <ShadowBox extraClass="game-users w-250">
                            <h2>Lista de usuarios</h2>
                            {
                                Object.keys(players).map((key) => (
                                    <h4 key={`u-${key}`}>{players[key].displayName}</h4>
                                ))
                            }
                        </ShadowBox>
                    )
                }
                <ShadowBox extraClass="game-options w-250">
                    <h2>Opciones del juego</h2>
                    {
                        !playing && <Button title="Jugar" extraClass="mt-10" onClick={handlePlay} />
                    }
                    {
                        playing && <Button title="Colocar barcos" extraClass="mt-10" onClick={handleReset} />
                    }
                    <Button title="Terminar partida" extraClass="mt-10" onClick={() => removeGame(id)} />
                </ShadowBox>
                {
                    !playing && (
                        <ShadowBox extraClass="game-pieces w-250">
                            <h2>Piezas</h2>
                            {
                                pieces && Object.keys(pieces).length > 0 && Object.keys(pieces).map((key) => (
                                    <div id={key} className="piece" key={`piece-${key}`}>
                                        <span className="w-200">{pieces[key].quantity}X Barco de {pieces[key].length} piezas</span>
                                        <Button title="Seleccionar" extraClass="w-100" onClick={() => setSelectedPiece(pieces[key])} />
                                    </div>
                                ))
                            }
                            <div className="piece">
                                <span className="w-200">Borrar casilla</span>
                                <Button title="Seleccionar" extraClass="w-100" onClick={() => setSelectedPiece({ color: 'white' })} />
                            </div>
                        </ShadowBox>
                    )
                }
                {
                    playing && (
                        <ShadowBox extraClass="game-pieces w-250">
                            <h2>Marcadores</h2>
                            {
                                markers && Object.keys(markers).length > 0 && Object.keys(markers).map((key) => (
                                    <div className="marker-container mt-10" id={key} key={`marker-${key}`}>
                                        <span className="w-200">{markers[key].label}</span>
                                        <div onClick={() => setSelectedMarker(markers[key])} className={`marker bg-${markers[key].color}`} />
                                    </div>
                                ))
                            }
                            <div className="marker-container mt-10">
                                <span className="w-200">Borrar casilla</span>
                                <div onClick={() => setSelectedMarker({ color: 'white' })} className={`marker bg-white`} />
                            </div>
                        </ShadowBox>
                    )
                }
                <Chat playerId={idPlayer} gameId={id} />
            </div>
        </div>
    )
}
