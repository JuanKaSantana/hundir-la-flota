import { useState } from 'react';
import { ShadowBox, Button, Loading } from '../components';
import { auth, firebase } from '../firebaseConfig';

function Login() {
    const [loadingUser, setLoadingUser] = useState(true)
    const [loading, setLoading] = useState(false)
    const loginWithProvider = () => {
        auth.languageCode = 'es';
        const GAP = new firebase.auth.GoogleAuthProvider();
        GAP.addScope('https://www.googleapis.com/auth/contacts.readonly');

        setLoading(true)
        auth.signInWithPopup(GAP)
            .then(() => {
                setLoading(false)
                window.location = '/home';
            }).catch((error) => {
                var errorCode = error.code;
                var errorMessage = error.message;
                var email = error.email;
                var credential = error.credential;
                console.error({
                    errorCode,
                    errorMessage,
                    email,
                    credential
                })
                setLoading(false)
            });
    }

    auth.onAuthStateChanged(function (user) {
        if (user) {
            window.location = "/home"
            return;
        }
        setLoadingUser(false)
    });
    if (loadingUser) {
        return <Loading />
    }


    return (
        <div className="login-container">
            {
                loading && <Loading />
            }
            {
                !loading && (
                    <ShadowBox extraClass="w-300 login-content">
                        <h1>Hundir la flota</h1>
                        <h4><i>JuanKa Santana</i></h4>
                        <Button title="Login" extraClass="w-200" onClick={loginWithProvider} />
                    </ShadowBox>
                )
            }
        </div>
    )
}

export default Login;
