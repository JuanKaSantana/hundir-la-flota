import { useState } from 'react';
import { Menu, Button, Loading } from '../components';
import { auth } from '../firebaseConfig';

function Profile() {
    const [loading, setLoading] = useState(false)
    const deleteAccount = () => {
        if (window.confirm('¿Seguro que desea eliminar su cuenta?')) {
            setLoading(true)
            auth.currentUser.delete()
                .then(() => {
                    window.location = "/login"
                    setLoading(false)
                })
        }
    }

    if (loading) {
        return <Loading />
    }

    return (
        <div className="profile">
            <Menu />
            <div className="profile-content">
                <Button title="Borrar cuenta" onClick={deleteAccount} />
            </div>
        </div>
    )
}

export default Profile;
